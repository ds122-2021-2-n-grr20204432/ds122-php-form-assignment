$(function(){
  $("#form-test").on("submit",function(){
    //nome
    nome_input = $("input[name='nome']");
    if(nome_input.val() == "" || nome_input.val() == null)
    {
      $("#erro-nome").html("O nome eh obrigatorio");
      return(false);
    }
    //email
    email_input = $("input[name='email']");
    if(email_input.val() == "" || email_input.val() == null) {
      $("#erro-email").html("O email é obrigatório.");
      return(false);
    }
    //data
    data_input = $("input[name='data']");
    if(data_input.val() == "" || data_input.val() == null) {
      $("#erro-data").html("Data de nascimento é obrigatório.");
      return(false);
    }
    //senha
    senha_input = $("input[name='senha']");
    if(senha_input.val() == "" || senha_input.val() == null) {
      $("#erro-senha").html("Senha é obrigatório.");
      return(false);
    }
    //confirmacao
    confirma_input = $("input[name='confirma']");
    if(confirma_input.val() == "" || confirma_input.val() == null) {
      $("#erro-confirma").html("Confirmação de senha é obrigatório.");
      return(false);
    }
    //imagem
    imagem_input = $("input[name='imagem']");
    if(imagem_input.val() == "" || imagem_input.val() == null) {
      $("#erro-imagem").html("O upload de imagem é obrigatório");
      return(false);
    }
    return(true);
  });
});
