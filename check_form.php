<?php
function verifica_campo($texto){
  $texto = trim($texto);
  $texto = stripslashes($texto);
  $texto = htmlspecialchars($texto);
  return $texto;
}
$nome = $email = $data = $senha = $confirma = $imagem = "";
$erro = false;
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  //nome
  if(empty($_POST["nome"])){
    $erro_nome = "Nome é obrigatório.";
    $erro = true;
  } else {
    $nome = verifica_campo($_POST["nome"]);
    if (!preg_match("/^[a-zA-Z-' ]*$/",$nome)) {
      $erro_nome = "Somente letras e espaços são permitidos";
      $erro = true;
    }
  }
  //email
  if(empty($_POST["email"])){
    $erro_email = "E-mail é obrigatório.";
    $erro = true;
  } else {
    $email = verifica_campo($_POST["email"]);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $erro_email = "Formato de e-mail inválido";
      $erro = true;
    }
  }
  //data de nascimento
  if(empty($_POST["data"])){
    $erro_data = "Data é obrigatório.";
    $erro = true;
  } else {
    $data = $_POST["data"];
    if ($data > date('Y-m-d')){
      $erro_data = "Data inválida";
      $erro = true;
    }
  }
  //senha
  if(empty($_POST["senha"])){
    $erro_senha = "Senha é obrigatório.";
    $erro = true;
  } else {
    $senha = verifica_campo($_POST["senha"]);
  }
  //confirmação de senha
  if(empty($_POST["confirma"])){
    $erro_confirma = "Confirmação de senha é obrigatório.";
    $erro = true;
  } else {
    $confirma = verifica_campo($_POST["confirma"]);
    if ($confirma != $senha){
      $erro_confirma = "Senha e confirmação de senha devem ser iguais.";
      $erro = true;
    }
  }
  //upload de imagem
  $imgDir = "../../imagens/";
  $imgTarget = $imgDir . basename($_FILES["imagem"]["name"]);
  $imgTmp = $_FILES["imagem"]["tmp_name"];
  $imgSize = $_FILES["imagem"]["size"];
  $imgType = strtolower(pathinfo($imgTarget,PATHINFO_EXTENSION));
  $allowedExt = array("jpg","png");
  if (in_array($imgType,$allowedExt)){
    if ($imgSize <= 1000000 && $imgSize <> 0){
      move_uploaded_file($imgTmp, $imgTarget);
    } else {
      $erro_imagem = "Erro. Tamanho deve ser menor do que 1MB.";
      $erro = true;
    }
  } else {
    $erro_imagem = "Extensão não aceita. Somente: jpg e png";
    $erro = true;
  }
}
?>